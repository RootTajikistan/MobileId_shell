#!/bin/bash

# Shell script for recognize Tajikistan mobile numbers
# Autor Firdavs Murodov <firdavs@murodov.net>
# Date Feb 17, 2015 15:02

if [ "$1" != "" ]; then
   input=$1
 else
   echo 'Please type phone number'
   read input
fi

number=`echo $input | sed -e  '1,$ s/^+992//g'|sed -e '1,$ s/-//g'`


if [[ $number =~ (^90|^55|^88)[0-9]{7}$ ]]; then
    echo "$number is Megafon number"

 elif [[ $number =~ (^918|^98[0-9])[0-9]{6}$ ]]; then
    echo "$number is Babilon number"

  elif [[ $number =~ (^93|^92|^50)[0-9]{7}$ ]]; then
     echo "$number is Tcell number"

  elif [[ $number =~ (^915|^917|^919)[0-9]{6}$ ]]; then
     echo "$number is Beeline number"

elif [[ $number =~ ^95[0-9]{7}$ ]]; then
   echo "$number is TK-Mobile number"

   else
    echo "$number Unknown number"
fi
