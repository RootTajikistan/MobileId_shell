# MobileId
Shell script for recognize Tajikistan mobile numbers

example:
```
$ ./mobileid.sh 918616161  
918616161 is Babilon number  
```
or  
```
$ ./mobileid.sh  
Please type phone number  
918616161  
918616161 is Babilon number  
```
